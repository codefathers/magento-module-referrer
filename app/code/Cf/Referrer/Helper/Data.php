<?php

/**
 * Codefathers Referrer Modul
 *
 * @copyright codefathers
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @version 1.0
 * @package Cf_Referrer
 *
 */
class Cf_Referrer_Helper_Data extends Mage_Core_Helper_Abstract
{

    /* @var string */
    protected $_baseUrl = null;
    
    
    /**
     * 
     * returns the basic app url 
     * with no protocoll information
     *
     * @return string 
     * 
     * 
     */
    protected function _getBaseUrl()
    {
        if (!isset($this->_baseUrl)) {
            $this->_baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
            $this->_baseUrl = $this->deleteTrailingDelimiters($this->_baseUrl);
            $this->_baseUrl = str_replace(array('https://', 'http://'), '', $this->_baseUrl);            
        }
        return $this->_baseUrl;
    }
    
    
    /**
     * 
     * 
     * Identify referrer url via all accepted methods (HTTP_REFERER, regular or base64-encoded request param)
     *
     * @return string
     */
    public function getReferrerUrl()
    {
        $request = Mage::app()->getRequest();
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_REFERER_URL)) {
            return $url;
        }
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_BASE64_URL)) {
            return Mage::helper('core')->urlDecode($url);
        }
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_URL_ENCODED)) {
            return Mage::helper('core')->urlDecode($url);
        }
        $result = $request->getServer('HTTP_REFERER');
        return $result;
    }

    /**
     * Check url to be used as internal
     *
     * @param   string $url
     * @return  bool
     */
    public function isUrlInternal($url)
    {
        $baseUrl = $this->_getBaseUrl();
        $url = str_replace(array('https://', 'http://'), '', $url);
        if (($n1 = strlen($url)) &&
                ($n2 = strlen($baseUrl)) &&
                ($n1 > $n2) &&
                (strpos($url, $baseUrl) === 0)) {
            return true;
        }
        return false;
    }

    /**
     * 
     * returns the last shop call from an external page
     * 
     * 
     * @return string
     * 
     */
    public function getLastExternalUrl()
    {
        $session = Mage::getSingleton('cf_referrer/session');
        return $session->getLastExternalUrl();
    }

    /**
     * 
     * returns the last shop call from an external page
     * 
     * 
     * @return string
     * 
     */
    public function getFirstExternalUrl()
    {
        $session = Mage::getSingleton('cf_referrer/session');
        return $session->getFirstExternalUrl();
    }

    /**
     * 
     * returns the last shop call from an external page
     * 
     * 
     * @return string
     * 
     */
    public function deleteTrailingDelimiters($str)
    {
        while (strlen($str) && (strrpos($str, '/') === (strlen($str) - 1))) {
            $str = substr($str, 0, strlen($str) - 1);
        }
        return $str;
    }

}
