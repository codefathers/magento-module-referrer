<?php

/**
 * Codefathers Referrer Modul
 *
 * @copyright codefathers
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @version 1.0
 * @package Cf_Referrer
 *
 */
class Cf_Referrer_Model_Observer
{

    /**
     * 
     * is invoked right before a controller will is dispatched
     * 
     * 
     * @param Varien_Event_Observer $observer
     * 
     */
    public function handleControllerActionPreDispatch(Varien_Event_Observer $observer)
    {
        /* do nothing on admin store */
        if (!Mage::app()->getStore()->getId()) {
            return ;
        }
        $session = Mage::getSingleton('cf_referrer/session');
        $helper = Mage::helper('cf_referrer');
        $url = $helper->getReferrerUrl();
        
        if ($url) {
            $session->setReferrerUrl($url);
            /* only external urls are of interrest */
            if (!$helper->isUrlInternal($url)) {
                /* allways set external url as last url */
                $session->setLastExternalUrl($url);
                (!$session->hasFirstExternalUrl()) && $session->setFirstExternalUrl($url);
            }
        }
    }

}

